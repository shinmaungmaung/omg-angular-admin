import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { serviceUrls } from 'src/app/app.config';

@Injectable()
export class ProductService {
  private readonly _baseUrl = serviceUrls.productsUrl;
  private readonly _searchUrl = `${this._baseUrl}/search`;

  constructor(private http: HttpClient) {}

  getAll(params?: { limit?: number; page?: number }) {
    return this.http.get(this._baseUrl, { params });
  }

  search(params: any) {
    return this.http.get(this._searchUrl, { params });
  }

  remove(id: string) {
    return this.http.delete(this._baseUrl + '/' + id);
  }
}
