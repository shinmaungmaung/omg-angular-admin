import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { fadeIn, fadeOut } from 'src/app/core/animations/fade';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [fadeIn, fadeOut],
})
export class ListProductsComponent implements OnInit {
  products$!: Observable<any[]>;

  formGroup!: FormGroup;

  isLoading!: boolean;

  limit: number = 10;

  page: number = 1;

  pages: number = 1;

  displayedColumns = ['name', 'price', 'category', 'created_by', 'actions'];

  dataSource = new MatTableDataSource([]);

  isSearched: boolean = true;

  selectedProduct!: any;

  @ViewChild('confirmModal') confirmModal!: TemplateRef<HTMLDivElement>;

  constructor(
    private productSerivce: ProductService,
    private fb: FormBuilder,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.formGroup = this.fb.group({ q: '' });
    this.reloadData(this.page);
  }

  reloadData(page: number) {
    this.page = page;
    if (this.isSearched) {
      return this.search(page);
    }
    this.isLoading = true;
    this.productSerivce
      .getAll({ limit: this.limit, page })
      .subscribe((data: any) => {
        this.isLoading = false;
        this.pages = data.pagable.pages;
        this.dataSource.data = data.result;
      }, this.handleError.bind(this));
  }

  search(page: number = 1) {
    if (!this.formGroup.value.q) {
      this.isSearched = false;
      this.reloadData(page);
      return;
    }
    this.isSearched = true;
    this.isLoading = true;
    this.productSerivce
      .search({
        ...this.formGroup.value,
        limit: this.limit,
        page: (this.page = page),
      })
      .subscribe((data: any) => {
        this.isLoading = false;
        this.pages = data.pagable.pages;
        this.dataSource.data = data.result;
      }, this.handleError.bind(this));
  }

  delete(product: any) {
    this.selectedProduct = product;
    this.dialog.open(this.confirmModal, {
      width: '380px',
      maxWidth: '100vw',
    });
  }

  confirmDelete() {
    let product = this.selectedProduct;
    this.isLoading = true;
    this.productSerivce
      .remove(product.id)
      .subscribe(() => this.reloadData(this.page), this.handleError.bind(this));
  }

  handleError(err: any) {
    this.isLoading = false;
  }
}
