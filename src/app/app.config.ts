import { NavigationItem } from './core/core.types';
import { FirebaseOptions } from 'firebase/app';
import { environment } from 'src/environments/environment';

export const firebaseConfig: FirebaseOptions = {
  apiKey: 'AIzaSyAaqKs26GeEpxcMFW2dbY_ePoVPvQdvXwo',
  authDomain: 'omg-baby-v2.firebaseapp.com',
  databaseURL: 'https://omg-baby-v2-default-rtdb.firebaseio.com',
  projectId: 'omg-baby-v2',
  storageBucket: 'omg-baby-v2.appspot.com',
  messagingSenderId: '1015182216299',
  appId: '1:1015182216299:web:a3c9c82a78412f15fa352e',
  measurementId: 'G-HS46PQ5GXJ',
};

export const serviceUrls = {
  productsUrl: `${environment.API_URL}/api/products`,
  categoriesUrl: `${environment.API_URL}/api/categories`,
};

export const navigationItems: NavigationItem[] = [
  {
    type: 'basic',
    title: 'Dashboard',
    icon: 'mat_outline:home',
    path: '/home',
  },
  {
    type: 'basic',
    title: 'Products',
    icon: 'mat_outline:info',
    path: '/products',
  },
];
