import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'x-paginator',
  templateUrl: './paginator.component.html',
})
export class PaginatorComponent {
  @Input() limit!: number;

  @Output() limitChange = new EventEmitter<number>();

  @Input() page!: number;

  @Output() pageChange = new EventEmitter<number>();

  @Input() pages!: number;
}
