export const environment = {
  production: false,
  TAILWIND_MODE: 'watch',
  API_URL: 'http://localhost:5000',
};
